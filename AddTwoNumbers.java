/**
 * Created by KG on 9/18/2015.
 */

import java.util.Scanner;

class AddNumbers
{
    public static void main(String args[])
    {
        int x, y, z;
        System.out.println("Enter two integers to calculate their sum ");

        Scanner in = new Scanner(System.in);
        String twonums = in.nextLine();
        String[] nums = twonums.split(" ");
        x = Integer.parseInt(nums[0]);
        y = Integer.parseInt(nums[1]);
        z = x + y;
        System.out.print("Sum of entered integers = "+z);
    }
}
