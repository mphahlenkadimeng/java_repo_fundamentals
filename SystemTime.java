import java.text.SimpleDateFormat;
import java.util.*;
/**
 * Created by KG on 9/23/2015.
 */
public class SystemTime {
    public static void main(String[] args) {

        Date date = new Date();
        String time = new SimpleDateFormat("HH:mm ss a").format(date);
        System.out.print(time);

    }
}