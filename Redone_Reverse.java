public class Reverse {
    public static void main(String[] args) {
        int arrnum[] = new int[]{5,10,20,45,67,80,90,100};

        System.out.println("printing original array");
        for (int i = 0; i < arrnum.length; i++) {
            System.out.println(arrnum[i]);

        }

        System.out.println("printing reversed array");
        for (int i = arrnum.length - 1; i >= 0; i--) {
            System.out.println(arrnum[i]);
        }
    }
}
