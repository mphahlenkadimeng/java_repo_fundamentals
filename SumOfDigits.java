import java.util.Scanner;

/**
 * Created by KG on 9/22/2015.
 */
public class SumOfDigits{

    public static void main(String args[]) {


        int lastdigit;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter a numbers to calculate sum of digits");
        int number = sc.nextInt(); int sum = 0;
        int input = number;
        while (input != 0)
        {
            lastdigit = input % 10;
            sum += lastdigit; input /= 10; }
        System.out.printf("Sum of digits of number ", number, sum);

    }
}