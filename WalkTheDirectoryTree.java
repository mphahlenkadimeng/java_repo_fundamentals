/**
 * Created by KG on 10/9/2015.
 */
import java.io.File;
import java.io.FileFilter;

/**
 * Created by KG on 10/9/2015.
 */
public class WalkTheDirectoryTree {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\KG\\Desktop\\Java\\java_repo_fundamentals\\my file");
        File[] fileList = file.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".txt");
            }
        });

        if (fileList != null) {

            for (File f : fileList) {
                System.out.println("File in directory: " + f.getName());
            }
        }
    }
}