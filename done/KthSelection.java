/**
 * Created by KG on 10/5/2015.
 */
public class KthSelection {
    public static void main(String[] args){
        int[] numbers = {5, 1, 3, 6, 2, 4};
        int pivot = 3;

        int selection = select(numbers, pivot);

        System.out.println("Selected number: " + selection);

    }

    public static int select(int[] arr, int k) {
        if (arr == null || arr.length <= k)
            throw new Error();

        int from = 0, to = arr.length - 1;


        while (from < to) {
            int r = from, w = to;
            int mid = arr[(r + w) / 2];


            while (r < w) {

                if (arr[r] >= mid) {
                    int tmp = arr[w];
                    arr[w] = arr[r];
                    arr[r] = tmp;
                    w--;
                } else {
                    r++;
                }
            }


            if (arr[r] > mid)
                r--;


            if (k <= r) {
                to = r;
            } else {
                from = r + 1;
            }
        }

        return arr[k];
    }
}