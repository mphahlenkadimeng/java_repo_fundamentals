/**
 * Created by Admin on 9/22/2015.
 */
public class BracketsBalance {
    public static void main(String[] args){

        for(int i = 0; i <= 10; i+=2){
            String brackets = generate(i);
            if (checkBrackets(brackets) == false)
                System.out.println(brackets + "\t" + "NOT OK");
            else if (checkBrackets(brackets) == true && i == 0)
                System.out.println("(empty)" + "\t" + "OK");
            else if (checkBrackets(brackets) == true)
                System.out.println(brackets + "\t" + "OK");
        }


    }
    public static boolean checkBrackets(String str){

        int mismatchedBrackets = 0;
        for(char ch:str.toCharArray()){
            if(ch == '['){
                mismatchedBrackets++;
            }else if(ch == ']'){
                mismatchedBrackets--;
            }else{
                return false ; //non-bracket chars
            }
            if(mismatchedBrackets < 0){ //close bracket before open bracket
                return false;
            }
        }

        return mismatchedBrackets == 0;
    }

    public static String generate(int n){
        String ans = "";
        int openBracketsLeft = n;
        int unclosed = 0;
        while(ans.length() < n){
            if(Math.random() >= .5 && openBracketsLeft > 0 || unclosed == 0){
                ans += '[';
                openBracketsLeft--;
                unclosed++;
            }else{
                ans += ']';
                unclosed--;
            }
        }
        return ans;
    }

}