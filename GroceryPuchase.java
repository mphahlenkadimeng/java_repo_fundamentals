import java.text.NumberFormat;

/**
 * Created by KG on 10/12/2015.
 */
public class GroceryPuchase {
    public static void main(String[] args){
        double total=7.11, itemPrice;
        NumberFormat formatter = NumberFormat.getCurrencyInstance();

        itemPrice = 1 + (1 / (total - 4));
        itemPrice = Math.round(itemPrice * 100)/100.0;

        String moneyString = formatter.format(itemPrice);

        System.out.println("The price of each item is: " + moneyString);

    }
}
