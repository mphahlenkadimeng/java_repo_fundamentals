import java.util.Random;
import java.util.Scanner;
/**
 * Created by KG on 10/2/2015.
 */
public class BullsAndCowsGame {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int bullScore = 0;
        int cowScore = 0;

        int[] originalNumbers = new int[4];
        int[] compareNumbers = new int[4];
        int[] userInput = new int[4];
        Random currentNumber = new Random();

        for (int guess = 0; guess < 4; guess++)
        {
            System.out.print("Please guess a number: ");
            userInput[guess] = in.nextInt();

            originalNumbers[guess] = currentNumber.nextInt(9) + 1;
            if (originalNumbers[guess] == userInput[guess]) {

                bullScore++;
                guess = 4;

            } else
                cowScore++;
        }

        for (int guess=0; guess < 4; guess++)
        {
            do {
                compareNumbers[guess] = currentNumber.nextInt(9) + 1;

            } while (compareNumbers[guess] == originalNumbers[guess]);
        }

        System.out.println("Bull Score: " + bullScore);
        System.out.println("Cow Score: " + cowScore);

    }
}