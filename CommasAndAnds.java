import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by KG on 9/23/2015.
 */
public class CommasAndAnds {

    public static void main(String[] args) throws Exception {
        final List<String> words = Arrays.asList(new String[]{"ABC", "DEF", "G", "H"});

        final Iterator<String> wordIterator = words.iterator();
        final StringBuilder out = new StringBuilder();
        while (wordIterator.hasNext()) {
            out.append(wordIterator.next());
            if (wordIterator.hasNext()) {
                out.append(",");
            }
        }
        System.out.println(out.toString());
    }
}