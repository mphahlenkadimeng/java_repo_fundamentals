import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
/**
 * Created by KG on 9/28/2015.
 */
public class ReadingXML {
    public static void main(String[] args) {

        try {
            File inputFile = new File("C:\\Users\\KG\\Desktop\\Java\\java_repo_fundamentals\\my file.xml");

            DocumentBuilderFactory create = DocumentBuilderFactory.newInstance();
            DocumentBuilder dCreate = create.newDocumentBuilder();
            Document doc = dCreate.parse(inputFile);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("Student");

            for (int temp = 0; temp < nList.getLength(); temp++){
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element eElement = (Element) nNode;

                    System.out.println( eElement.getAttribute("Name"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}