/**
 * Created by CHARLIE on 2015-09-17.
 */
public class _99BottlesOfBeer {
    public static void main(String[] args) {
        for(int i=99; i>=0; i-- )
        {
            System.out.println(i + " bottles of beer on the wall\n" +
                    i + " bottles of beer\n" +
                    "Take one down, pass it around\n");
        }

    }
}
