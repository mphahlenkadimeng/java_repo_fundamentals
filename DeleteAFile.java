/**
 * Created by KG on 10/5/2015.
 */

    import java.io.File;
    public class DeleteAFile {
        public static void main(String[] args){
            try{
                File folder = new File("C:\\Users\\KG\\Desktop\\Java\\java_repo_fundamentals.balance.txt");

                if(folder.delete()){
                    System.out.println("the folder " + folder.getName() + " Was deleted!");
                }else{
                    System.out.println("Folder Delete Operation Failed. Check: " + folder);
                }
            }catch(Exception e1){
                e1.printStackTrace();
            }

            try{
                File file = new File("C:\\Users\\KG\\Desktop\\Java\\java_repo_fundamentals.balance.txt");

                if(file.delete()){
                    System.out.println("the file " + file.getName() + " Was deleted!");
                }else{
                    System.out.println("File Delete Operation processed. Check: " + file);
                }
            }catch(Exception e1){
                e1.printStackTrace();
            }

        }
    }