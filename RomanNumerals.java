import java.util.Scanner;
/**
 * Created by KG on 10/7/2015.
 */

public class RomanNumerals {

    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String sRoman = (""); // holds Roman numerals

        System.out.println("This program converts numbers");
        System.out.println("ranging from 1 - 3,000");
        System.out.println("to Roman numerals.");
        System.out.println();

        System.out.print("Enter a number (between 1 - 3,000): ");
        int iInput = console.nextInt();
        if (iInput <= 0 || iInput >= 3000) {
            throw new NumberFormatException("Numbers need to be between 1 - 3,000.");
        }

        while (iInput >= 1000) {
            iInput = iInput - 1000;
            sRoman += ("M");
        }
        if (iInput >= 900) {
            iInput = iInput - 900;
            sRoman += ("CM");
        }
        if (iInput >= 500) {
            iInput = iInput - 500;
            sRoman += ("D");
        }
        if (iInput >= 400) {
            iInput = iInput - 400;
            sRoman += ("CD");
        }
        if (iInput >= 100) {
            iInput = iInput - 100;
            sRoman += ("C");
        }
        if (iInput >= 90) {
            iInput = iInput - 90;
            sRoman += ("XC");
        }
        if (iInput >= 50) {
            iInput = iInput - 50;
            sRoman += ("L");
        }
        if (iInput >= 40) {
            iInput = iInput - 40;
            sRoman += ("XL");
        }
        if (iInput >= 10) {
            iInput = iInput - 10;
            sRoman += ("X");
        }
        if (iInput >= 9) {
            iInput = iInput - 9;
            sRoman += ("IX");
        }
        if (iInput >= 5) {
            iInput = iInput - 5;
            sRoman += ("V");
        }
        if (iInput >= 4) {
            iInput = iInput - 4;
            sRoman += ("IV");
        }
        if (iInput >= 1) {
            iInput = iInput - 1;
            sRoman += ("I");
        }

        System.out.println("The Roman numeral equivalent is: " + (String) sRoman);
    }
}