import java.util.Arrays;
import java.util.Scanner;

public class Reverse {
    public static void main(String[] args) {


        String str, rev;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the string : ");
        str = in.nextLine();
        rev = new StringBuffer(str).reverse().toString();
        System.out.println("\nString before reverse:" + str);
        System.out.println("String after reverse:" + rev);


        int[] numbers = {1, 2, 3, 4, 5, 6, 7};

        reverse(numbers);
    }
    public static void reverse(int[] input) {
        System.out.println("original array : " + Arrays.toString(input));

        if(input == null || input.length <= 1){
            return;
        }
        for (int i = 0; i < input.length / 2; i++) {
            int temp = input[i];
            input[i] = input[input.length - 1 - i];
            input[input.length - 1 - i] = temp; }
        System.out.println("reversed array : " + Arrays.toString(input));


}
}
