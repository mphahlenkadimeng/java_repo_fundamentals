/**
 * Created by KG on 9/22/2015.
 */
public class SumOfNumbers {


    public static void main(String[] args) {

        System.out.print("Program that calculates numbers in the array");

        int numbers [] = new int[]{10,20,30,40,50,60,70,80,90,100};
        int sum =0;


            for (int i = 0; i < numbers.length; i++)
            {
                sum += numbers[i];
            }


        System.out.println("The sum is " + sum);


        }
}

