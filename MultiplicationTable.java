/**
 * Created by KG on 9/21/2015.
 */
public class MultiplicationTable {
    public static void main(String[] args) {

        int c, n = 1;
        System.out.println("multiplication table\n");
        do {
            for (c = 1; c <= 12; c++) {

                System.out.println(n + " * " + c + " = " + (n * c));
            }
            n++;
            System.out.println("\n");


        } while (n <= 12);
    }
}
